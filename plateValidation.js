function isValidPlate(plate) { // eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  var result = (plate.match(re) != null);
  gtag('event', 'click', { // eslint-disable-line
    'event_category' : 'is_valid',
    'event_label': result
  });
  return result;
}