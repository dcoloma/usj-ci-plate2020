test("Passing lower case letters and discarding letters and numbers with opposite order", function (assert) {
    assert.equal(isValidPlate(1234 + "BCD"), true, "4 NUMBERS AND 3 LETTERS");
    assert.equal(isValidPlate(1234 + "bcd"), true, "NORMAL NUMBER AND LOWER CASE LETTERS");
    assert.equal(isValidPlate("BCD" + 1234), false, "LETTERS WHERE NUMBERS AND NUMBERS WHERE LETTERS");
});

test("More or less arguments than expected", function (assert) {
    assert.equal(isValidPlate(123 + "BCD"), false, "3 NUMBERS AND 3 LETTERS");
    assert.equal(isValidPlate(1234 + "BC"), false, "4 NUMBERS AND 2 LETTERS");
    assert.equal(isValidPlate(01234 + "BCD"), false, "'0' + 4 NUMBERS AND 3 LETTERS");
    //assert.equal(isValidPlate(12345 + "BCD"), false, "5 NUMBERS AND 3 LETTERS");
    //assert.equal(isValidPlate(1234 + "BCDF"), false, "4 NUMBERS AND 4 LETTERS");
});

test("Invalid arguments", function (assert) {
    assert.equal(isValidPlate(1234 + "AEI"), false, "ALL VOWELS");
    assert.equal(isValidPlate(1234 + "QBC"), false, "1 'Q' INVALID CHARACTER");
    assert.equal(isValidPlate(1234 + "qbc"), false, "1 'q' INVALID CHARACTER");
    assert.equal(isValidPlate(1234 + "ÑBC"), false, "1 'Ñ' INVALID CHARACTER");
    assert.equal(isValidPlate(1234 + "ñbc"), false, "1 'ñ' INVALID CHARACTER");
    assert.equal(isValidPlate(1234 + "ABC"), false, "1 VOWEL");
    assert.equal(isValidPlate("@#€¬" + "BCD"), false, "NOT NUMBER CHARACTERS");
    assert.equal(isValidPlate(1234 + "@#€"), false, "NOT LETTER CHARACTERS");
});

test("Trying with all number combinations and all letter combinations", function (assert) {
    var allNumbers = true;
    for (var i = 0; i < 10000; ++i)
    {
        var currentNum = i.toString(10);
        if (i < 10)
        {
            currentNum = "000" + currentNum;
        }
        else if (i < 100)
        {
            currentNum = "00" + currentNum;
        }
        else if (i < 1000)
        {
            currentNum = "0" + currentNum;
        }

        if (!isValidPlate(currentNum + "BCD"))
        {
            allNumbers = false;
            break;
        }
    }        
    assert.equal(allNumbers, true, "ALL VALID NUMBERS COMBINATIONS");
    
    var allLetters = true;
    var validLetters = "BCDFGHJKLMNPRSTVWXYZ";
    for (var i = 0; i < validLetters.length; ++i)
    {
        var first = String.fromCharCode(validLetters.charCodeAt(i));
        for (var j = 0; j < validLetters.length; ++j)
        {
            var second = String.fromCharCode(validLetters.charCodeAt(j));
            for (var k = 0; k < validLetters.length; ++k)
            {
                var third = String.fromCharCode(validLetters.charCodeAt(k));

                if (!isValidPlate("1234" + first + second + third))
                {
                    allLetters = false;
                    validLetters = first + second + third;
                    break;
                }
            }
        }
    }
    assert.equal(allLetters, true, "ALL VALID LETTERS COMBINATIONS");
});